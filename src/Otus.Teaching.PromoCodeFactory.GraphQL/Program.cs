using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.GraphQL.Customers;
using Otus.Teaching.PromoCodeFactory.GraphQL.Types;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<CustomersQueries>();

builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();
builder.Services.AddDbContext<DataContext>(x =>
    {
        x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
    });

builder.Services
    .AddGraphQLServer()
    .AddQueryType<CustomersQueries>()
    .AddMutationType<CustomersMutations>()
    .AddType<CustomerType>()
    .AddType<PreferenceType>();

var app = builder.Build();

var serviceProvider = app.Services;
using (var scope = serviceProvider.CreateScope())
{
    var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
    dbInitializer.InitializeDb();
}

app.MapGraphQL();

app.Run();