﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Types;

public class PreferenceType: ObjectType<Preference> {
    protected override void Configure(IObjectTypeDescriptor<Preference> descriptor) {
        descriptor.Field(p => p.Id).Type<NonNullType<UuidType>>();
        descriptor.Field(p => p.Name).Type<NonNullType<StringType>>();
    }
}