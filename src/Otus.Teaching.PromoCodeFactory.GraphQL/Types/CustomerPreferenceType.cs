﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Types;

public class CustomerPreferenceType: ObjectType<CustomerPreference> {
    protected override void Configure(IObjectTypeDescriptor<CustomerPreference> descriptor) {
        descriptor.Field(p => p.PreferenceId).Type<NonNullType<UuidType>>();

        descriptor.Field("preference")
            .Type<PreferenceType>()
            .Resolve(context =>
                {
                    var customerPreference = context.Parent<CustomerPreference>();
                    var preference = customerPreference.Preference;
                    return preference;
                });
    }
}